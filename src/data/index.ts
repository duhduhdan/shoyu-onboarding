export const tokens = {
  "0xd746347eff950754507c2e3337fe963d76be465a": {
    symbol: "CVX",
  },
  "0x0ead1160bd2ca5a653e11fae3d2b39e4948bda4d": {
    symbol: "SUSHI",
  },
  "0xc7ad46e0b8a400bb3c915120d284aafba8fc4735": {
    symbol: "DAI",
  },
  "0xbb5a50e0b7e964cc6591b7528d021710b79f740f": {
    symbol: "ALPHA",
  },
  "0x9cb36c6039890304ecde1a0e57ea331b630adf7a": {
    symbol: "PERP",
  },
  "0x971f469f6e98556d3b24d0427102cd3d6c60ee91": {
    symbol: "AAVE",
  },
  "0x3ee4e7dc0c4616c8da2f38089269755c8d1ecaf1": {
    symbol: "CRV",
  },
  "0x12110717f1bdc62e154b2a2c46586d18f90af20c": {
    symbol: "LINK",
  },
  "0xc778417e063141139fce010982780140aa0cd5ab": {
    symbol: "WETH",
  },
  "0x6eb433f3512589568a68a9582cf0ae52afa7f234": {
    symbol: "MKR",
  },
}
