import { Web3ReactProvider, useWeb3React } from "@web3-react/core"
import { InjectedConnector } from "@web3-react/injected-connector"
import { Web3Provider } from "@ethersproject/providers"

import { TokenList } from "./components/TokenList"
import { MigrateLP } from "./components/MigrateLP"
import { tokens } from "./data"

const CHAIN_IDS: { [k: number]: string } = {
  "1": "Ethereum",
  "3": "Ropsten",
  "4": "Rinkeby",
  "5": "Goerli",
  "42": "Kovan",
}

const injected = new InjectedConnector({
  supportedChainIds: [1, 3, 4, 5, 42],
})

function getLibrary(provider: any) {
  return new Web3Provider(provider)
}

function App() {
  const context = useWeb3React<Web3Provider>()
  const { activate, account, active, chainId } = context

  return (
    <div className="min-h-screen max-w-max p-8 my-0 mx-auto">
      <header className="flex items-center justify-end h-16 mb-8">
        {active ? (
          <div className="flex items-center space-x-4">
            {chainId && chainId === 4 ? (
              <>
                <div className="bg-black">
                  <span className="block font-bold text-white p-2">
                    {CHAIN_IDS[chainId]}
                  </span>
                </div>
                <span className="text-xl font-bold">Account:</span> {account}
              </>
            ) : null}
            {chainId && chainId !== 4 ? (
              <div className="text-lg">
                <span className="font-bold">{CHAIN_IDS[chainId]}</span> is not
                yet supported! Please use{" "}
                <span className="font-bold">{CHAIN_IDS[4]}</span>.
              </div>
            ) : null}
          </div>
        ) : (
          <button
            onClick={() => activate(injected)}
            className="rounded-lg bg-pink-500 text-white p-4"
          >
            Connect with MetaMask
          </button>
        )}
      </header>
      <div className="grid grid-cols-3 gap-8">
        <div className="space-y-6 col-span-1">
          <TokenList tokens={tokens} />
        </div>
        <div className="col-span-2">
          <div className="flex flex-col space-y-6 bg-gray-300 p-12">
            <h1 className="font-bold text-2xl underline">
              Migrate tokens from Uniswap
            </h1>
            <MigrateLP tokens={tokens} />
          </div>
        </div>
      </div>
    </div>
  )
}

const WalletApp = () => (
  <Web3ReactProvider getLibrary={getLibrary}>
    <App />
  </Web3ReactProvider>
)

export default WalletApp
