import { BigNumber, FixedNumber } from "@ethersproject/bignumber"
import { UseToken } from "../hooks/useToken"

export async function slippage(
  LP_TOKEN: UseToken,
  TOKEN0: UseToken,
  TOKEN1: UseToken,
  lpTokens: string,
  lpTokenAddress: string
) {
  // Grab the balance of LP token address for EACH token in pair
  // E.g. DAI.balanceOf(LPTokenAddress), WETH.balanceOf(LPTokenAddress)
  // Then grab balance of LP Tokens for wallet.address (account)
  // E.g. liquidity = LPTokens.balanceOf(account) ACTUALLY this should be the liquidity input requesting to move. but this will at least prevent someone from inputing more than they have.
  // Then figure out how much of each token for an LP position
  // E.g. DAI.balanceOf(LPTokenAddress).mul(liquidity)
  // Now, build in slippage for amountAMin -> DAI.balanceOf(LPTokenAddress).mul(liquidity).mul(0.995) (0.5%)
  const tenpow18 = BigNumber.from("10").pow("18")
  const totalSupply = await LP_TOKEN.totalSupply()
  // All of token0 that LP Token owns
  const lpOwnsTokens0 = await TOKEN0.balanceOf(lpTokenAddress)
  // All of token1 that LP Token owns
  const lpOwnsTokens1 = await TOKEN1.balanceOf(lpTokenAddress)
  // Total position for account
  // const liquidity = await LP_TOKEN.balanceOf(account as string)
  // LP Position represents this many of token0
  const lpPositionOwnsToken0 = lpOwnsTokens0
    .mul(FixedNumber.from(lpTokens))
    .div(totalSupply)
  // LP Position represents this many of token1
  const lpPositionOwnsToken1 = lpOwnsTokens1
    .mul(FixedNumber.from(lpTokens))
    .div(totalSupply)

  const amountAMin = lpPositionOwnsToken0
    .mul(FixedNumber.from("0.995"))
    .div(tenpow18)

  const amountBMin = lpPositionOwnsToken1
    .mul(FixedNumber.from("0.995"))
    .div(tenpow18)

  return [amountAMin.toString(), amountBMin.toString()]
}
