import { useRef } from "react"
import { IconCopy } from "../Icon/Copy"
import { useCopy } from "../../hooks/useCopy"

export type Props = {
  name: string
  address: string
}

// TODO(dan): Add a neat "copied!" feedback
export function Token({ name, address }: Props) {
  const addressRef = useRef<HTMLDivElement>(null)
  const { copy } = useCopy(addressRef)

  return (
    <li className="flex items-center justify-between p-4 text-lg border-2 border-black rounded-lg space-x-4">
      <div className="font-bold text-right">{name}</div>{" "}
      <div ref={addressRef}>{address}</div>
      <button
        onClick={copy}
        className="flex items-center text-black hover:text-blue-500"
      >
        <div className="mr-1">
          <IconCopy />
        </div>{" "}
        <span className="text-sm">Copy address</span>
      </button>
    </li>
  )
}
