import { useEffect, useState } from "react"
import { BigNumber, FixedNumber } from "@ethersproject/bignumber"
import { formatUnits } from "@ethersproject/units"
import dayjs from "dayjs"

import { useActiveWeb3React } from "../../hooks/useWeb3React"
import { useSushiRoll } from "../../hooks/useSushiRoll"
import { useUniswapV2Pair } from "../../hooks/useUniswapV2Pair"
import { useSigning } from "../../hooks/useSigning"
import { usePair } from "../../hooks/usePair"
import { useToken } from "../../hooks/useToken"

import { slippage } from "../../utils/calculateSlippage"
import { SUSHI_ROLL_RINKEBY_ADDRESS } from "../../utils/constants"

import { TextInput } from "../TextInput"

type Tokens = Record<string, Record<"symbol", string>>

type Props = {
  tokens: Tokens
}

type MigrateToken = { address: string; symbol: string }

type State = {
  token0: MigrateToken
  token1: MigrateToken
  lpTokens: string
}

export function MigrateLP({ tokens }: Props) {
  const [value, setValue] = useState<BigNumber | null>(null)
  const [sign, setSign] = useState<boolean>(false)
  const [fields, setFields] = useState<State>({
    token0: {
      address: "",
      symbol: "",
    },
    token1: {
      address: "",
      symbol: "",
    },
    lpTokens: "",
  })
  const [balanceForAccount, setBalanceForAccount] = useState<string>("")
  const [lpPositionGTbalanceForAccount, setLpPositionGTbalanceForAccount] =
    useState<boolean>(false)

  const { account, chainId } = useActiveWeb3React()
  const SushiRoll = useSushiRoll()
  const pair = useUniswapV2Pair()
  const { signPermit } = useSigning({
    owner: (account as string) || "",
    spender: SUSHI_ROLL_RINKEBY_ADDRESS,
    value: value as BigNumber,
  })
  const {
    getPair,
    lpToken: lpTokenAddress,
    token0Address,
    token1Address,
    pairNotFound,
  } = usePair({
    token0: fields.token0,
    token1: fields.token1,
  })
  const TOKEN0 = useToken(token0Address)
  const TOKEN1 = useToken(token1Address)
  const LP_TOKEN = useToken(lpTokenAddress)

  useEffect(() => {
    async function getPosition() {
      const balance = await LP_TOKEN.balanceOf(account as string)

      setBalanceForAccount(formatUnits(balance))
    }

    if (lpTokenAddress && account) {
      getPosition()
    }
  }, [lpTokenAddress, account])

  function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    const { value, name } = e.target

    const [token] = name.split("MigrateAddr")

    setFields((fields) => ({
      ...fields,
      [token]: {
        ...(fields as any)[token],
        address: value,
        symbol: (tokens as any)?.[value]?.symbol || "",
      },
    }))
  }

  function updateLpTokens(e: React.ChangeEvent<HTMLInputElement>) {
    const { value } = e.target

    setFields((fields) => ({
      ...fields,
      lpTokens: value,
    }))

    if (value) {
      setLpPositionGTbalanceForAccount(
        BigNumber.from(FixedNumber.from(value)).gt(
          BigNumber.from(FixedNumber.from(balanceForAccount))
        )
      )

      setValue(BigNumber.from(FixedNumber.from(value)))
    }
  }

  async function handleApprove() {
    await pair.approve({
      spender: SUSHI_ROLL_RINKEBY_ADDRESS,
      value: fields.lpTokens,
    })
  }

  async function handleMigrate() {
    const { token0, token1 } = fields

    const [amountAMin, amountBMin] = await slippage(
      LP_TOKEN,
      TOKEN0,
      TOKEN1,
      fields.lpTokens,
      lpTokenAddress
    )

    await SushiRoll.migrate({
      tokenA: token0.address,
      tokenB: token1.address,
      liquidity: fields.lpTokens,
      amountAMin,
      amountBMin,
    })
  }

  async function handleMigrateWithSign() {
    const deadline = dayjs().add(10, "minutes").unix()
    const { token0, token1 } = fields
    const [amountAMin, amountBMin] = await slippage(
      LP_TOKEN,
      TOKEN0,
      TOKEN1,
      fields.lpTokens,
      lpTokenAddress
    )
    const ownerNonce = await pair.nonces(account)
    const { v, r, s } = await signPermit(deadline, ownerNonce)

    if (v && r && s) {
      await SushiRoll.migrateWithPermit({
        tokenA: token0.address,
        tokenB: token1.address,
        liquidity: fields.lpTokens,
        amountAMin,
        amountBMin,
        deadline,
        v,
        r,
        s,
      })
    }
  }

  return (
    <>
      <div className="flex flex-col space-y-8">
        <div className="text-lg">
          <span className="font-bold">LP position:</span>{" "}
          <span>
            {balanceForAccount
              ? balanceForAccount
              : pairNotFound
              ? "You don't have any LP tokens for this pair! Try another one."
              : "---"}
          </span>
        </div>
        <TextInput
          label="token0 address"
          name="token0MigrateAddr"
          value={fields.token0.address}
          onChange={handleChange}
          symbol={fields.token0.symbol}
          disabled={chainId !== 4}
        />
        <TextInput
          label="token1 address"
          name="token1MigrateAddr"
          value={fields.token1.address}
          onChange={handleChange}
          symbol={fields.token1.symbol}
          disabled={chainId !== 4}
        />
        <div className="space-y-2">
          <TextInput
            label="amount of LP tokens to migrate"
            name="lpTokens"
            value={fields.lpTokens}
            onChange={updateLpTokens}
            disabled={
              !fields.token0.address ||
              !fields.token1.address ||
              !account ||
              !lpTokenAddress ||
              chainId !== 4
            }
          />
          <div className="text-red-500 font-bold text-lg">
            {lpPositionGTbalanceForAccount
              ? "You don't have that many LP tokens!"
              : null}
          </div>
        </div>
      </div>
      <div className="flex w-full justify-between space-x-6">
        <button
          onClick={getPair}
          disabled={
            !fields.token0.address ||
            !fields.token1.address ||
            !account ||
            chainId !== 4
          }
          className="w-full bg-black disabled:bg-gray-500 disabled:cursor-not-allowed text-white text-xl py-4 rounded-lg"
        >
          Get liquidity position
        </button>
        <button
          disabled={
            !fields.token0.address ||
            !fields.token1.address ||
            !fields.lpTokens ||
            !account ||
            !lpTokenAddress ||
            lpPositionGTbalanceForAccount ||
            pair.approving ||
            chainId !== 4
          }
          className="w-full bg-black disabled:bg-gray-500 disabled:cursor-not-allowed text-white text-xl py-4 rounded-lg"
          onClick={
            !pair.approved
              ? handleApprove
              : sign
              ? handleMigrateWithSign
              : handleMigrate
          }
        >
          {!pair.approved
            ? pair.approving
              ? "Approving..."
              : "Approve"
            : "Migrate to the far superior SushiSwap platform"}
        </button>
      </div>
      <div className="flex w-full items-end justify-end">
        <label className="flex items-center">
          <input
            type="checkbox"
            name="sign"
            onChange={(e) => setSign(e.target.checked)}
            className="mr-2 disabled:cursor-not-allowed"
            disabled={chainId !== 4}
          />
          Sign with MetaMask?
        </label>
      </div>
    </>
  )
}
