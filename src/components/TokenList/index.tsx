import { Token } from "../Token"

type Tokens = Record<string, Record<"symbol", string>>

type Props = {
  tokens: Tokens
}

export function TokenList({ tokens }: Props) {
  return (
    <ul className="flex flex-col space-y-8">
      {Object.keys(tokens).map((token) => (
        <Token
          name={tokens[token].symbol}
          address={token}
          key={`${tokens[token].symbol}--${token}`}
        />
      ))}
    </ul>
  )
}
