import { ChainId, Token, Fetcher } from "@uniswap/sdk"
import { useState } from "react"

import { useActiveWeb3React } from "../hooks/useWeb3React"

type LP = Record<"token0" | "token1", Record<"address" | "symbol", string>>

export function usePair({ token0, token1 }: LP) {
  const { chainId } = useActiveWeb3React()

  const [lpToken, setLpToken] = useState<string>("")
  const [token0Address, setToken0Address] = useState<string>("")
  const [token1Address, setToken1Address] = useState<string>("")

  const [pairNotFound, setPairNotFound] = useState<boolean>(false)

  async function getPair() {
    try {
      setPairNotFound(false)

      const TOKEN0 = new Token(
        chainId || ChainId.MAINNET,
        token0.address,
        18,
        token0.symbol
      )

      const TOKEN1 = new Token(
        chainId || ChainId.MAINNET,
        token1.address,
        18,
        token1.symbol
      )

      const tokens = await Fetcher.fetchPairData(TOKEN0, TOKEN1)

      setToken0Address(token0.address)

      setToken1Address(token1.address)

      setLpToken(tokens.liquidityToken.address)
    } catch (e) {
      setPairNotFound(true)
      console.error("That pair does not exist or I'm bad at coding.")
    }
  }

  return {
    getPair,
    lpToken,
    pairNotFound,
    token0Address,
    token1Address,
  }
}
