import { useCallback } from "react"
import { BigNumber, FixedNumber } from "@ethersproject/bignumber"
import dayjs from "dayjs"

import { useSushiRollContract } from "./useContract"

type Migrate = {
  tokenA: string
  tokenB: string
  liquidity: string
  amountAMin: string
  amountBMin: string
}

type MigrateWithPermit = Migrate & { deadline: number; r: any; s: any; v: any }

export function useSushiRoll() {
  const sushiRollContract = useSushiRollContract()

  const migrate = useCallback(
    async ({ tokenA, tokenB, liquidity, amountAMin, amountBMin }: Migrate) => {
      const deadline = dayjs().add(10, "minutes").unix()

      if (!tokenA || !tokenB || !liquidity) {
        return
      }

      try {
        await sushiRollContract?.migrate(
          tokenA,
          tokenB,
          BigNumber.from(FixedNumber.from(liquidity)),
          BigNumber.from(amountAMin),
          BigNumber.from(amountBMin),
          deadline,
          { gasLimit: 1000000 }
        )
      } catch (e) {
        console.error("Migrating failed!", e)
      }
    },
    [sushiRollContract]
  )

  const migrateWithPermit = useCallback(
    async ({
      tokenA,
      tokenB,
      liquidity,
      amountAMin,
      amountBMin,
      deadline,
      v,
      r,
      s,
    }: MigrateWithPermit) => {
      if (!tokenA || !tokenB || !liquidity) {
        return
      }

      try {
        await sushiRollContract?.migrateWithPermit(
          tokenA,
          tokenB,
          BigNumber.from(FixedNumber.from(liquidity)),
          BigNumber.from(amountAMin),
          BigNumber.from(amountBMin),
          deadline,
          v,
          r,
          s,
          { gasLimit: 1000000 }
        )
      } catch (e) {
        console.error("Migrating failed!", e)
      }
    },
    [sushiRollContract]
  )

  return {
    migrate,
    migrateWithPermit,
  }
}
