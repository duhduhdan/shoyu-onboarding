import { BigNumber, FixedNumber } from "@ethersproject/bignumber"
import { useCallback, useEffect, useState } from "react"

import { useUniswapV2PairContract } from "./useContract"

export function useUniswapV2Pair() {
  const [approved, setApproved] = useState<boolean>(false)
  const [approving, setApproving] = useState<boolean>(false)
  const uniswapV2PairContract = useUniswapV2PairContract()

  useEffect(() => {
    uniswapV2PairContract?.on("Transfer", () => {
      window.location.reload()
    })
  }, [uniswapV2PairContract])

  const approve = useCallback(
    async ({ spender, value }) => {
      try {
        setApproving(true)

        const tx = await uniswapV2PairContract?.approve(
          spender,
          BigNumber.from(FixedNumber.from(value)),
          { gasLimit: 1000000 }
        )

        if (tx) {
          uniswapV2PairContract?.on("Approval", (address, spender, value) => {
            if (address && spender && value) {
              setApproved(true)
              setApproving(false)
            }
          })
        }
      } catch (e) {
        console.error("Failed to approve amount", e)
      }
    },
    [uniswapV2PairContract]
  )

  const nonces = useCallback(
    async (address) => {
      try {
        const nonces = await uniswapV2PairContract?.nonces(address)

        return nonces
      } catch (err) {
        console.error("Can't get nonce", err)
      }
    },
    [uniswapV2PairContract]
  )

  return { approve, approved, approving, nonces }
}
