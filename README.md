# Shoyu Onboarding

## Instructions

- `yarn && yarn start`

## TODO

- [X] token list
- [X] uniswap LP
	- [X] copy/paste address into `token0` and `token1` inputs
	- [X] `getPair` hook for retrieving LP information
	- [X] display LP token amount
- [X] migrate
	- [X] basic UI for migrate section
	- [X] interact with contract w/ hooks
	- [X] debug why transaction is reverting
      - First thought it was an invalid liquidity amount (it sort of is)
      - Actually need to approve first before migrating
	- [X] implement slippage for `tokenAMin` and `tokenBMin` in `SushiRoll.migrate()`
- [X] sign migration with metamask

## Notes

- Be sure whatever browser you're using supports clipboard extensions in order to copy with a button
- Rinkeby is the only chain supported right now

## Dependencies and tools

- `create-react-app`
- `tailwindcss`
- `@uniswap/sdk`
- `@uniswap/v2-sdk`
- `ethers`
- `dayjs`
- `@web3-react/core`
- `@web3-react/injected-connector`

## Time tracking

| Session | Time     | Notes                                                                                                                                                                                                                                                                                   |
| ------- | -------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 10/06   | 1h 21min | Stood up project; tailwind; created token list; first pass @ uniswap LP                                                                                                                                                                                                                 |
| 10/07   | 43min    | Componentized inputs; made LP component; made hook for LP pair; refined uniswap LP                                                                                                                                                                                                      |
| 10/07   | 29min    | Re-order how tokens are stored (use address as key); display symbol after pasting address; styling updates to input components                                                                                                                                                          |
| 10/08   | 1h 16min | Finally figure out how tf to get reserves of token in pair                                                                                                                                                                                                                              |
| 10/08   | 26min    | UI for migrate tokens                                                                                                                                                                                                                                                                   |
| 10/09   | 2h 18min | SushiRoll ABI, hooks for interacting with contract (a few taken from `sushiswap/sushiswap-interface`), `Web3Provider`, connect wallet w/ `web3-react/injected-connector`, styling updates, `NumberInput` component, bunch of other things in diff that I'm too lazy to type out in full |
| 10/10   | 26min    | `gasLimit`, input lp token amount _not_ `token0` or `token1` amount.                                                                                                                                                                                                                    |
| 10/11   | 56min    | Get migrating...somewhat working. Transactions are sent through successfully, but it always reverts on `ds-sub-math-underflow`. Need to look at making sure I'm submitting right liquidity value (I think I am)                                                                         |
| 10/11   | 25min    | Implement `approve` from `UniswapV2Pair` to make `migrate` actually feasible                                                                                                                                                                                                            |
| 10/11   | 10min    | Migrate with 0 `amountAMin` and `amountBMin`. Small UI update for approving state                                                                                                                                                                                                       |
| 10/12   | 1h 22min | Bunch of nerd shit to get slippage working. But it works now.                                                                                                                                                                                                                           |
| 10/13   | 1h 26min | EIP-712 stuff                                                                                                                                                                                                                                                                           |
| 10/14   | 1h 41min | Finish up everything                                                                                                                                                                                                                                                                    |
